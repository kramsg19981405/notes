## kubernetes

###### Raspberry Pi cluster setup steps:

sudo vim /boot/cmdline.txt
Add: cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory

sudo dphys-swapfile swapoff
sudo dphys-swapfile uninstall
sudo apt purge dphys-swapfile

curl -sSL get.docker.com | sh
sudo usermod -aG docker jay

On every node:

sudo vim /etc/docker/daemon.json:

{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}

Reboot


sudo vim /etc/apt/sources.list.d/kubernetes.list
Add: deb http://apt.kubernetes.io/ kubernetes-xenial main

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

sudo apt update

sudo apt install kubeadm kubectl kubelet

Master-only:
Note: Probably should be declaring a network here with the init command
kubeadm init --pod-network-cidr=10.17.0.0/16 --service-cidr=10.18.0.0/24 --service-dns-domain=home-network.io
Linux Academy version is shorter: sudo kubeadm init --pod-network-cidr=10.244.0.0/16
Without declaring a network: sudo kubeadm init --token-ttl=0

Note: You will get some output that will include the join command, but don't join nodes yet

On the master:
mkdir -p ~.kube
sudo cp /etc/kubernetes/admin.conf ~/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

Install flannel network driver (lack of sudo is iintentional):
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

Make sure all the pods come up:
kubectl get pods --all-namespaces

Once they do, run the join command on the other nodes

Run this command to see if they join:
kubectl get nodes

###### Reinitialize
On all nodes:
`kubeadm reset`

rm ~/.kube/config
sudo cp /etc/kubernetes/admin.conf ~/.kube/config
chown jay: .,ube/config

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

###### Check logs of a pod

`kubectl logs -n kube-system kube-flannel-ds-arm-sp94q`
