## Openstack

###### Single-node installation (MicroStack method)

`sudo apt install snapd`
`sudo snap install microstack --classic --edge`
`sudo microstack.init`

At this point, Openstack should be accessible from a web console.

User: admin
Password: keystone


###### Cloud images

Ubuntu 18.04:
http://uec-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img

###### Single-node installation (Kolla method)

Note: The below section is incomplete, perhaps some of the info will be helpful if this method is attempted again, but this will need to be redone.

#### Install KVM:

`sudo apt-get install ansible git`

#### Set up bridge:

Note: This may be completely unnecessary, since the article was using virutalization.

Edit /etc/netplan/01-netcfg.yaml

```network:
  version: 2
  renderer: networkd
  ethernets:
    eno1:
      dhcp4: no
  bridges:
    br0:
      dhcp4: yes
      interfaces:
        - eno1
```

#### Install killa-ansible

`sudo pip install kolla-ansible --ignore-installed`

#### Set up Git and clone the Kolla repositories

`git config --global user.name "Your Name"`
`git config --global user.email "your@github-email"`
`git clone https://github.com/openstack/kolla`
`git clone https://github.com/openstack/kolla-ansible`

#### Copy recommended configuration files and install requirements
`sudo cp -r /usr/share/kolla-ansible/etc_examples/kolla /etc/`
`sudo cp /usr/share/kolla-ansible/ansible/inventory/* .`
`sudo pip install -r kolla/requirements.txt`
`sudo pip install -r kolla-ansible/requirements.txt`

#### Copy the configuration files

`sudo mkdir -p /etc/kolla`
`sudo cp -r kolla-ansible/etc/kolla/* /etc/kolla`
`sudo cp kolla-ansible/ansible/inventory/* .`

#### Create LVM Cinder Volume

`sudo lvcreate -n lv_cinder -L 60GB openstack-vg`

#### Edit Kolla Global config file

Uncomment and change the following lines:

config_strategy: "COPY_ALWAYS"
kolla_base_distro: "ubuntu"
kolla_install_type: "binary"
openstack_release: "master"
network_interface: "eno1"
neutron_external_interface: "eno2"
keepalived_virtual_router_id: "51"
enable_cinder: "yes"
enable_cinder_backend_lvm: "yes"
enable_heat: "yes"
enable_haproxy: "no"

kolla_internal_address: "172.16.249.5"
Note: The last line was not present and had to be added in order to avoid it complaining that it was undefined.

#### Generate passwords

`sudo kolla-ansible/tools/generate_passwords.py`
`grep keystone_admin_password /etc/kolla/passwords.yml`

#### Install Heat packages

`sudo pip install openstack-heat`

#### Bootstrap the Kolla containers

`cd kolla-ansible`

`sudo kolla-ansible -i all-in-one bootstrap-servers`
`sudo kolla-ansible -i all-in-one pre-checks`

Note: If the second command complains about ssl-match-hostname, run:

`sudo pip uninstall backports.ssl-match-hostname`
`sudo apt install python-backports.ssl-match-hostname`
