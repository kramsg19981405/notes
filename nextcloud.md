## Nextcloud

#### Required packages (Ubuntu 18.04)
  `apache2 libapache2-mod-php7.2 php7.2-xml php7.2-cgi php7.2-mysql php7.2-mbstring php7.2-gd php7.2-curl php7.2-zip`

#### Required apache modules
  * env
  * dir
  * headers
  * mime
  * php7.2
  * proxy
  * proxy_http
  * proxy_wstunnel
  * rewrite
  * ssl

#### Installing Collabora Office
  * `a2enmod proxy`
  * `a2enmod proxy_wstunnel`
  * `a2enmod proxy_http`
  * `apt-get install docker.io`
  * `docker run -t -d -p 127.0.0.1:9980:9980 -e 'domain=nextcloud\\.mydomain\\.net' --restart always --cap-add MKNOD collabora/code`
  * Add virtual host file (below)
  * `sudo certbot --installer apache --authenticator webroot -d office.mydomain.net`
     Note: When the command asks for document root, give it the existing nextcloud document root

#### Virtual host file for collabora:

    <VirtualHost *:443>
      ServerName office.mydomain.net:443

      # Encoded slashes need to be allowed
      AllowEncodedSlashes NoDecode

      # keep the host
      ProxyPreserveHost On

      # static html, js, images, etc. served from loolwsd
      # loleaflet is the client part of LibreOffice Online
      ProxyPass           /loleaflet https://127.0.0.1:9980/loleaflet retry=0
      ProxyPassReverse    /loleaflet https://127.0.0.1:9980/loleaflet

      # WOPI discovery URL
      ProxyPass           /hosting/discovery https://127.0.0.1:9980/hosting/discovery retry=0
      ProxyPassReverse    /hosting/discovery https://127.0.0.1:9980/hosting/discovery

      # Main websocket
      ProxyPassMatch "/lool/(.*)/ws$" wss://127.0.0.1:9980/lool/$1/ws nocanon

      # Admin Console websocket
      ProxyPass   /lool/adminws wss://127.0.0.1:9980/lool/adminws

      # Download as, Fullscreen presentation and Image upload operations
      ProxyPass           /lool https://127.0.0.1:9980/lool
      ProxyPassReverse    /lool https://127.0.0.1:9980/lool

      # Container uses a unique non-signed certificate
      SSLProxyEngine On
      SSLProxyVerify None
      SSLProxyCheckPeerCN Off
      SSLProxyCheckPeerName Off

    </VirtualHost>

#### Test auto-discovery for sync URLs
Visit https://cloud.mydomain.com/remote.php/dav/ and you should see a message about the service only being for the desktop sync client.

#### DAVDroid Syncing
  * URL: `https://nextcloud.mydomain.tld/remote.php/dav/`
    (If nextcloud is a subdirectory, the URL would be:
    `https://server.tld/nextcloud/remote.php/dav/`
  * Mods to enable in Apache: headers env dir mime rewrite
  * Make sure the below configuration is added to the nextcloud virtual host:
		RewriteEngine on
        Redirect 301 /.well-known/carddav /remote.php/dav
        Redirect 301 /.well-known/caldav /remote.php/dav

        <Directory "/var/www/nextcloud.mydomain.tld">
                Options +FollowSymlinks
                Options Indexes MultiViews
                AllowOverride all
                Order allow,deny
                allow from all
                <IfModule mod_dav.c>
                    Dav off
                </IfModule>

                SetEnv HOME /var/www/nextcloud.mydomain.tld
                SetEnv HTTP_HOME /var/www/nextcloud.mydomain.tld
        </Directory>

## Previous Issues

#### "Authorization Failed" error while using Synology Cloud Sync
Add /remote.php/webdav to the URL
Note: I wouldn't think this should be necessary.

#### "Request entity too large" error when uploading files
Edit the nginx config, and in the server { section, place the following:

    client_max_body_size        10G;
    client_body_buffer_size     400M;

#### e-book reader not saving bookmarks
    * Add the following lines to /var/www/nextcloud.domain.net/config/config.php:
        'overwrite.cli.url' => 'https://nextcloud.domain.net',
        'htaccess.RewriteBase' => '/',
    * Run the following command:
        `sudo -u www-data php /var/www/nextcloud.domain.net/occ maintenance:update:htaccess`

#### Difficulties syncing contacts/calendar behind proxy
  Some combination of the below worked:
  Added the following configuration items to config.php:
    'overwrite.cli.url' => 'https://cloud.home-network.io',
    'overwritehost' => 'cloud.home-network.com',
    'overwriteprotocol' => 'https',
    'htaccess.RewriteBase' => '/',
    "trusted_proxies" => ['172.16.249.4'],
    "overwritehost" => "cloud.home-network.io",
  Followed the following article (namely enablint remoteip mod in apache, followed the configuration):
    https://www.techandme.se/set-up-nginx-reverse-proxy/
  Used the following URL for both calendar/contacts (auto-discovery worked in MacOS but not iOS):
    https://cloud.mydomain.com/remote.php/dav/principals/users/jay

#### URL for syncing contacts to macOS
  Use the following URL for adding contacts to macOS:
  `https://cloud.mydomain.com/remote.php/dav/principals/users/jay`

####  memcache errors in config page
  * install: `php-apcu`
  * Edit: `/var/www/nextcloud/config/config.php`
     Add: `'memcache.local' => '\OC\Memcache\APCu'`
